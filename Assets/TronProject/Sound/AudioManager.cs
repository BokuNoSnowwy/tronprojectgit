﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Audio;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    
    void Awake()
    {
        foreach (var s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }
    
    void Start()
    {
        PlaySound("PlayerDeath");
    }
    
    void Update()
    {
        
    }

    public void PlaySound(string name)
    {
        Sound soundToPlay = Array.Find(sounds, sound => sound.name == name);
        soundToPlay?.source.Play();
    }
}

[Serializable]
public class Sound
{
    public string name;
    
    public AudioClip clip;

    public bool loop;

    [Range(0, 1)] 
    public float volume;

    [Range(0.1f, 1f)] 
    public float pitch;
    
    [HideInInspector]
    public AudioSource source;
}