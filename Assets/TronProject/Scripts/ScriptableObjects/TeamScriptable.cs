﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Team", menuName = "Tron/Team")]
public class TeamScriptable : ScriptableObject
{
    public int idTeam;
    public Color color;
}


[CreateAssetMenu(fileName = "Bike", menuName = "Tron/Bike")]
public class BikeScriptable : ScriptableObject
{
    public Sprite bikeImage;
    public NameBike name;
}

public enum NameBike
{
    Panthers,
    Tron,
    Mono
}