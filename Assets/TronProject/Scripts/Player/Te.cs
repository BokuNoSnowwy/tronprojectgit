﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Te : MonoBehaviour
{
    public LineRenderer lineRenderer;

    public int index;

    public float timeBetweenNewPoints;

    public int maxPoints;

    private Mesh mesh;

    public MeshCollider meshCollider;

    //public EdgeCollider2D edgeCollider2D;
    
    //public List<Vector2> linePos = new List<Vector2>();
    
    void Start()
    {
        //mesh = new Mesh();

        lineRenderer.positionCount = maxPoints;
        index = maxPoints - 1;

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            lineRenderer.SetPosition(i, transform.position);
        }
        
        StartCoroutine(NewPoint());
    }
    
    void Update()
    {
        
    }

    public IEnumerator NewPoint()
    {
        yield return new WaitForSeconds(timeBetweenNewPoints);
        //Debug.Log("New Point");
        //Index need to be = to 0 && 1 position set to 000 in the lineRenderer
        
        //Set the new Point pos
        lineRenderer.positionCount++;
        index++;
        lineRenderer.SetPosition(index, gameObject.transform.position);

        RaycastHit raycastHit;
        
        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            if (i + 1 < lineRenderer.positionCount && Physics.Linecast(lineRenderer.GetPosition(i), lineRenderer.GetPosition(i + 1), out raycastHit))
            {
                Debug.Log("Collision : " + raycastHit.collider.gameObject);
            }
        }
        
        //Update the mesh
        /*lineRenderer.BakeMesh(mesh);
        meshCollider.sharedMesh = mesh;*/

        //Update the edgeCollider
        /*Vector3 pointPos = lineRenderer.GetPosition(index);
        linePos.Add(new Vector2(pointPos.x, pointPos.z));
        edgeCollider2D.points = linePos.ToArray();*/
        
        //Check if remove
        if (lineRenderer.positionCount >= maxPoints)
        {
            RemoveLastPoint();
        }

        //Loop
        if (index < lineRenderer.positionCount)
        {
            StartCoroutine(NewPoint());
        }
    }

    public void RemoveLastPoint()
    {
        //Debug.Log("Remove Point");
        
        //remove the last Point for edge2D
        //linePos.RemoveAt(0);
        
        for (int i = 0; i < lineRenderer.positionCount - 1; i++)
        {
            lineRenderer.SetPosition(i, lineRenderer.GetPosition(i + 1));
        }

        index--;
        lineRenderer.positionCount--;
    }
}
