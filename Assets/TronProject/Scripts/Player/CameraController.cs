﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float cameraHorizontal;
    public float cameraReset;
    public Camera playerCamera;
    public float cameraSpeed;
    public Vector3 startPos , startRot;
    public PlayerStatsTron _playerStats;

    //take new input button joystick right
    public void OnCameraHorizontal(InputAction.CallbackContext ctx) => cameraHorizontal = ctx.ReadValue<float>();
    public void OnUseResetCamera(InputAction.CallbackContext ctx) => cameraReset = ctx.ReadValue<float>();

    void Awake()
    {
        startPos = playerCamera.transform.localPosition;
        startRot = playerCamera.transform.localEulerAngles;
    }
    void Start()
    {
        if (GameplayManagerTron.Instance)
        {
            int playerCount = GameplayManagerTron.Instance.playerListController.Count;
            //take value player count in the game
      
            //take start pose of the camera
            startPos = playerCamera.transform.localPosition;
            startRot = playerCamera.transform.eulerAngles;
            if (playerCount == 1)
            {
                playerCamera.rect = new Rect(0f, 0f, 1f, 1f);;
            }
            //if 2 player the screen divise the screen height by 2
            else if (playerCount == 2)
            {
                playerCamera.rect = new Rect(0f, 0.5f-_playerStats.idPlayer / playerCount * 0.5f, 1f, 0.5f);;
            }
            //if 4 players the screen divise the scren width and height by 2
            else if (playerCount >= 3)
            {
                playerCamera.rect = new Rect((_playerStats.idPlayer + 1) % 2 * 0.5f, 0.5f - (_playerStats.idPlayer - 1) / 2 * 0.5f, 0.5f, 0.5f);
            }
        }
    }
    void Update()
    {
        //if joystick aren't touch the speed = 0
        if (cameraHorizontal == 0)
        {
            cameraSpeed = 0f;
        }
        //if joystick value is <0 camera turn around lef sens
        if (cameraHorizontal < 0)
        {
            cameraSpeed = 100f;
            playerCamera.transform.RotateAround(transform.position, Vector3.up, cameraSpeed * Time.deltaTime);
        }
        //if joystick value is >0 camera turn around right sens
        if(cameraHorizontal > 0)
        {
            cameraSpeed = 100f;
            playerCamera.transform.RotateAround(transform.position, Vector3.down, cameraSpeed * Time.deltaTime);
        }
        //if button push the camera become starpose postion and rotation
        if(cameraReset == 1)
        {
            Debug.Log("reset");
            playerCamera.transform.localPosition = startPos;
            playerCamera.transform.localEulerAngles = startRot; 
        }
    }
}
