﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsTron : MonoBehaviour
{

    public int health;
    public int maxHealth = 100;
    public float turbo;
    public float maxTurbo = 50;
    public int idPlayer;


    public int idPlayerPrefab;
    public int idTeam;
    public Color colorTeam;
    private PlayerController _playerController;
    private CameraController _cameraController;


    //TODO faire la team
    
    public TrailScript trailScript;
    
    [Header("Team/Bike Management")]
    public Teams team;
    public int idBike;
    public BikeScriptable bike;
    public Material materialURPLit;
    
    [Header("Trail Management")]
    [SerializeField] private LineRenderer _lineRenderer;
    
    [Header("Sprite Mini Map Management")]
    [SerializeField] private SpriteRenderer _spriteRendererMiniMap;



    // Start is called before the first frame update
    void Start()
    {
        _playerController = GetComponent<PlayerController>();
        _cameraController = GetComponent<CameraController>();
        
        
       
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void Setup()
    {
        SetupBike();
        
        _lineRenderer = GetComponentInChildren<LineRenderer>();
        _spriteRendererMiniMap = GetComponentInChildren<SpriteRenderer>();
        
        SetupTrailColor();
        SetupSpriteMiniMap();
        
        trailScript = GetComponentInChildren<TrailScript>();

    }

    public void SetupSpriteMiniMap()
    {
        if (_spriteRendererMiniMap)
        {
            Color colorToAdd = team.colorTeam;
            colorToAdd.a = 1;
            _spriteRendererMiniMap.color = colorToAdd;
        }
    }
    

    public void SetupBike()
    {
        //GameObject bikeMesh = Instantiate(GameplayManagerTron.Instance.meshBikeList[idBike], gameObject.transform);
        _cameraController.playerCamera.transform.parent = gameObject.transform;
        _playerController.charaController = GetComponentInChildren<CharacterController>();


        for (int j = 0; j < gameObject.transform.childCount; j++)
        {
            if (gameObject.transform.GetChild(j).GetComponent<MeshRenderer>())
            {
                foreach (var material in gameObject.transform.GetChild(j).GetComponent<MeshRenderer>().materials)
                {
                    if (material.name == materialURPLit.name + " (Instance)")
                    {
                        //Change the color of the team
                        material.SetColor("_EmissionColor",team.colorTeam*2f);
                        material.SetColor("_BaseColor",team.colorTeam*2f);
                    }
                }
            }
        }
    }
    
    public void SetupTrailColor()
    {
        if (_lineRenderer)
        {
            _lineRenderer.materials[0].SetColor("Color_42E85C2F",team.colorTeam);
        }
    }
    
    
    public void Damage(int damage)
    {
        if (health - damage <= 0)
        {
            health = 0;
            Death();
        }
        else
        {
            health -= damage;
        }
    }
    
    public void Damage(int damage,PlayerStatsTron enemy)
    {
        if (health - damage <= 0)
        {
            health = 0;
            if (enemy == this)
            {
                Death();
            }
            else
            {
                Killed(enemy);
            }
        }
        else
        {
            health -= damage;
        }
    }

    public void AddTurbo(float value)
    {
        turbo += value;
        if (turbo > maxTurbo)
        {
            turbo = maxTurbo;
        }
    }

    public void Respawn()
    {
        turbo = maxTurbo;
        health = maxHealth;
        _playerController.ResetPlayer();
        //gameObject.transform.rotation = Quaternion.Euler(Vector3.zero);
        
        //Reset Trail
        trailScript.ResetTrail();
        //TODO faire un look at vers le milieux de l'arene
    }

    public void Death()
    {
        GameplayManagerTron.Instance.GivePoint(-1,team);
        GameplayManagerTron.Instance.DisablePlayer(this);
    }
    
    public void Killed(PlayerStatsTron enemy)
    {
        GameplayManagerTron.Instance.GivePoint(1,enemy.team);
        GameplayManagerTron.Instance.DisablePlayer(this);
    }
}
