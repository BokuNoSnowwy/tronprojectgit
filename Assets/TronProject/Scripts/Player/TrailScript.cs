﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailScript : MonoBehaviour
{
    public LineRenderer lineRenderer;

    public int index;

    public float timeBetweenNewPoints;

    public int maxPoints;


    
    void Start()
    {
      
        
        ResetTrail();
        StartCoroutine(NewPoint());
    }
    
    void Update()
    {
        
    }

    

    public IEnumerator NewPoint()
    {
        yield return new WaitForSeconds(timeBetweenNewPoints);
        //Index need to be = to 0 && 1 position set to 000 in the lineRenderer
        
        //Set the new Point pos
        if (index < lineRenderer.positionCount)
        {
            lineRenderer.positionCount++;
            index++;
            lineRenderer.SetPosition(index, gameObject.transform.position);
        }
        
        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            RaycastHit raycastHit;
            
            if (i + 1 < lineRenderer.positionCount && Physics.Linecast(lineRenderer.GetPosition(i), lineRenderer.GetPosition(i + 1), out raycastHit))
            {
                if (raycastHit.collider.gameObject.GetComponentInChildren<PlayerController>())
                {
                    PlayerController player = raycastHit.collider.gameObject.GetComponentInChildren<PlayerController>();
                    player.TrailHit();
                }
                //Debug.Log("Collision : " + raycastHit.collider.gameObject);
            }
        }

        //Check if remove
        if (lineRenderer.positionCount >= maxPoints)
        {
            RemoveLastPoint();
        }


        StartCoroutine(NewPoint());
        
    }

    public void RemoveLastPoint()
    {
        //Debug.Log("Remove Point");

        for (int i = 0; i < lineRenderer.positionCount - 1; i++)
        {
            lineRenderer.SetPosition(i, lineRenderer.GetPosition(i + 1));
        }
        index--;
        lineRenderer.positionCount--;
    }

    public void ResetTrail()
    {
        lineRenderer.positionCount = maxPoints;
        index = maxPoints - 1;

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            lineRenderer.SetPosition(i, transform.position);
        }
    }

    private void OnEnable()
    {
        ResetTrail();
        StartCoroutine(NewPoint());
    }
}
