﻿using System;
using System.Collections;
using System.Collections.Generic;
using TreeEditor;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public PlayerStatsTron playerStatsTron;
    
    //JumpForce
    [SerializeField] private float fallMultiplier = 2f;
    
    public CharacterController charaController;
    public float gravity = -9.81f;
    
    //Sprint
    /*
    public int speed = 6;
    [SerializeField] private int sprintSpeed = 9;
    [SerializeField] private int baseSpeed = 6;
    */

    [Header("Player Speed")]
    //Normal spd
    public float baseSpd = 2;
    public float speed;
    public float maxSpd = 10;
    public float acceleration = 0.3f;
    public float breakValue = 1.5f;
    //public bool onBreak;

    //Turbo spd
    [Header("Player Turbo")]
    public bool onTurbo;
    public float turboBaseSpd = 0;
    public float turboSpd = 0;
    public float turboMaxSpd = 10;
    public float turboAcceleration = 1f;
    public float turboConsommationMultiplier;

    //Global spd
    public float globalSpeed;
    
    [Header("Inertia")] 
    public float inertiaFactor;
    [SerializeField] private float baseRotationSensivity = 5;
    [SerializeField] private float rotationSensivity = 5;

    [Header("Collision Management")] 
    [SerializeField]
    public float hitDamageValueObstacle;
    [SerializeField] private float hitDamageValueTrail;
    [SerializeField] public bool hasRecentlyCollided;


    [SerializeField] private bool isBumped;
    [SerializeField] private float timeLerpBumpMultiplier;
    [SerializeField] private float distanceBumpMultiplier;

    [Header("Physics")]
    private Vector3 _velocity;
    private Quaternion _targetRotation;
    
    //Axis
    [Header("Player Input Axis Values")]
    [SerializeField] private float horizontal;
    [SerializeField] private float vertical;
    [SerializeField] private float turboButton;
    


    private void Awake()
    {
        charaController = GetComponentInChildren<CharacterController>();
        _targetRotation = transform.rotation;
    }

    public void Start()
    {
        playerStatsTron = GetComponent<PlayerStatsTron>();
        /*
        playerInput = new MovePlayer();
        playerInput.Player1.SetCallbacks(this);
        playerInput.Player1.Enable();
        */
        
        speed = baseSpd;

        foreach (var gamepad in Gamepad.all)
        {
            Debug.Log(gamepad.name);
        }
    }


    // Update is called once per frame
    private void Update()
    {

        //Inertia Simulation
        rotationSensivity = baseRotationSensivity - (speed + turboSpd)/(inertiaFactor + speed + turboSpd);

        //Enable a smooth rotation of the Y axis relateted to the horizontal axis
        if (horizontal != 0) 
        { 
            _targetRotation *=  Quaternion.AngleAxis(rotationSensivity, new Vector3(0f,horizontal,0f));
        }
        transform.rotation= Quaternion.Lerp (transform.rotation, _targetRotation , 10 * TimeManager.deltaTime);
        
        //Spd management of both turbo and normal speed
        PlayerTurboManagement();
        PlayerSpeedManagement();
        
        //Clamp the value to disable potential bugs
        turboSpd = Mathf.Clamp(turboSpd, turboBaseSpd, turboMaxSpd);
        speed = Mathf.Clamp(speed, baseSpd, maxSpd);

        //Value of the current spd
        globalSpeed = speed + turboSpd;
        //Make the caracter moves even if the acceleration button is not pressed
        if (!isBumped)
        {
            if (charaController)
            {
                charaController.Move(transform.forward * (globalSpeed * TimeManager.deltaTime));
                //transform.Translate(transform.forward * (globalSpeed * TimeManager.deltaTime));
                _velocity.y += gravity * TimeManager.deltaTime * fallMultiplier;
                charaController.Move(_velocity *TimeManager.deltaTime);
                //transform.Translate(_velocity *TimeManager.deltaTime);
            }
        }
    }
    
   

    public void TrailHit()
    {
        Debug.Log("Hit");
        
        //float bumpDistancePerSpeed =Mathf.Clamp(globalSpeed * distanceBumpMultiplier,0.1f,1f);;
        if (!hasRecentlyCollided)
        {
            playerStatsTron.Damage(Mathf.FloorToInt(globalSpeed * hitDamageValueTrail));
            Debug.Log("Inflige : " + globalSpeed * hitDamageValueTrail);
            hasRecentlyCollided = true;
            if (gameObject.activeSelf)
            {
                //StartCoroutine(CollisionBump(1,bumpDistancePerSpeed,(i) => { isBumped = i;} ));
                ResetSpeed();
                StartCoroutine(ResetBool( false, 2f , (i) => { hasRecentlyCollided = i;} ) );
            }
        }
    }

    public void PlayerSpeedManagement()
    {
        //Acceleration Management of the player
        if (speed <= maxSpd  && speed >= baseSpd)
        {
            if (vertical > 0 || onTurbo)
            {
                speed += acceleration * TimeManager.deltaTime;

            }
            else if(vertical == 0)
            {
                speed -= acceleration * TimeManager.deltaTime;
                if (speed < baseSpd)
                {
                    speed = baseSpd;
                }
            }
            else if(vertical < 0)
            {
                if (speed != baseSpd)
                {
                    speed -= (acceleration + breakValue) * TimeManager.deltaTime;
                    if (speed < baseSpd)
                    {
                        speed = baseSpd;
                    }
                }
            }
        }
    }

    public void PlayerTurboManagement()
    {
        if (turboButton > 0 && playerStatsTron.turbo > 0)
        {
            onTurbo = true;
            playerStatsTron.turbo -= TimeManager.deltaTime * turboConsommationMultiplier;
            
            if (playerStatsTron.turbo < 0)
            {
                playerStatsTron.turbo = 0;
            }
            
            if (turboSpd < turboMaxSpd)
            {
                
                turboSpd += turboAcceleration * TimeManager.deltaTime;
            }
        }
        else
        {
            onTurbo = false;
            turboSpd -= turboAcceleration * TimeManager.deltaTime;
        }

    }

    public void ResetSpeed()
    {
        speed = baseSpd;
        turboSpd = turboBaseSpd;
    }


    public void ResetPlayer()
    {
        hasRecentlyCollided = false;
        isBumped = false;
        ResetSpeed();
    }

    public void ReduceSpeed(float value)
    {
        speed -= value;
        turboSpd -= value * 2;
    }
    
    //Coroutine that make the player bumb when he enters a wall/trail
    public IEnumerator CollisionBump(float timeBump, float distanceBump, System.Action<bool> callback)
    {
        //Put the bool isBumped on true
        callback(true);
        
        float elapsedTime = 0;
        //Find the transform position behind the player, making him go toward its position
        Vector3 pointToGo = transform.position + (-transform.forward * distanceBump);

        //Do a lerp during the TimeBump seconds
        while (elapsedTime < timeBump)
        {
            transform.position = Vector3.Lerp (transform.position, pointToGo, elapsedTime/timeBump);
            elapsedTime += TimeManager.deltaTime;

            yield return null;
        }
        transform.position = pointToGo;
        //Reset player's speed
        ResetSpeed();
        
        //Put the bool isBumped on false after timeBump seconds
        callback(false);
        
        yield return null;
    }

    public static IEnumerator ResetBool( bool value, float timeToModif, System.Action<bool> callback)
    {
        Debug.Log("CoroutineStart");
        yield return new WaitForSeconds(timeToModif);
        Debug.Log("CoroutineEnd");
        callback(value);
    }
    //New Input System Management
    public void OnHorizontalMove(InputAction.CallbackContext ctx) => horizontal = ctx.ReadValue<float>();
    public void OnVerticalMove(InputAction.CallbackContext ctx) => vertical = ctx.ReadValue<float>();
    public void OnUseTurbo(InputAction.CallbackContext ctx) => turboButton = ctx.ReadValue<float>();
    public void OnUseResetCamera(InputAction.CallbackContext context)
    {
        
    }

    public void OnCameraMove(InputAction.CallbackContext context)
    {
        
    }
    


    
}
