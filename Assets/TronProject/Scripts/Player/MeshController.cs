﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshController : MonoBehaviour
{
    private PlayerController _playerController;
    // Start is called before the first frame update
    void Start()
    {
        _playerController = GetComponentInParent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
     //Collision gestion of either the trail and the obstacles
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (!_playerController.hasRecentlyCollided)
        {
            //float bumpDistancePerSpeed =Mathf.Clamp(_playerController.globalSpeed * _playerController.distanceBumpMultiplier,0.1f,1f);;
            //float bumpTimePerSpeed = Mathf.Clamp(_playerController.globalSpeed * _playerController.timeLerpBumpMultiplier,0.1f,0.3f);

            if (hit.gameObject.layer == LayerMask.NameToLayer("Obstacles"))
            {
                //Debug.DrawLine(hit.point, (hit.point - hit.gameObject.transform.right * 5f) , Color.green,10f);

                Debug.DrawLine(gameObject.transform.position, transform.position + transform.right * 2f, Color.green,10f);
                Debug.DrawLine(gameObject.transform.position, transform.position - transform.right * 2f, Color.green,10f);


                RaycastHit hitRaycast;
                LayerMask layer = LayerMask.GetMask("Obstacles");
                
                _playerController.hasRecentlyCollided = true;
                StartCoroutine(PlayerController.ResetBool( false, 2f , (i) => { _playerController.hasRecentlyCollided = i;} ) );
                _playerController.ResetSpeed();
                //StartCoroutine(CollisionBump(bumpTimePerSpeed,bumpDistancePerSpeed,(i) => { isBumped = i;} ));
                
                _playerController.playerStatsTron.Damage(Mathf.FloorToInt(_playerController.globalSpeed * _playerController.hitDamageValueObstacle));
                Debug.Log("Inflige : " + _playerController.globalSpeed * _playerController.hitDamageValueObstacle);
                
                
                if (Physics.Raycast(transform.position, transform.position + transform.right, out hitRaycast,
                    Mathf.Infinity, layer))
                {

                    /*
                    if (hitRaycast.distance == 0)
                    {
                        Debug.Log("Inflige : " + globalSpeed * hitDamageValueObstacle);
                        hasRecentlyCollided = true;
                        StartCoroutine(ResetBool( false, 2f , (i) => { hasRecentlyCollided = i;} ) );
                        StartCoroutine(CollisionBump(bumpTimePerSpeed,bumpDistancePerSpeed,(i) => { isBumped = i;} ));
                    }
                    else
                    {
                        Debug.Log("Inflige : " + globalSpeed * hitDamageValueObstacle);
                        hasRecentlyCollided = true;
                        StartCoroutine(ResetBool( false, 2f , (i) => { hasRecentlyCollided = i;} ) );
                        ReduceSpeed(globalSpeed * 0.25f);
                    }
                    
                    Debug.Log("Distance : " + hitRaycast.distance);
                    */
                }
            }
        }
    }
}
