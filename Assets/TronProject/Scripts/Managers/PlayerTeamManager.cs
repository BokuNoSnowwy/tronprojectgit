﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerTeamManager : MonoBehaviour
{
        
    public static PlayerTeamManager Instance;
    
    public List<PlayerStatsTron> playerList = new List<PlayerStatsTron>();
    public List<Teams> teamList = new List<Teams>();

    public List<int> intListController = new List<int>();
    public List<int> intListKeyboard = new List<int>();
    
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
        
        DontDestroyOnLoad(this);
    }

    public void SetupTeams()
    {
        //For each player on the list
        for (int i = 0; i < playerList.Count; i++)
        {
            //If the team list does not containt the team of the player
            if (!teamList.Contains(playerList[i].team))
            {
                //Create a new team and assign its teamBase and it's name to the gameplayManager
                Teams newTeam = new Teams("Equipe " + (i+1), playerList[i].colorTeam, 0);
                
                //UI Creation
                //GameObject scoreUI = Instantiate(scoreTextPrefab, whitePanelParent);
                //scoreUI.GetComponent<ScoreUI>().team = newTeam;
                
                //Attribute created team to player
                playerList[i].team = newTeam;
                
                //Add the player on the team list
                teamList.Add(playerList[i].team);
            }
        }
    }

    public void PopulatePlayerList(PlayerStatsTron playerToAdd)
    {
        if (!playerList.Contains(playerToAdd))
        {
            playerList.Add(playerToAdd);
        }

    }
}
[System.Serializable]
public class Teams
{
    public string nameTeam;
    public Color colorTeam;
    public int scoreTeam;

    public Teams(string nameTeam, Color colorTeam, int scoreTeam)
    {
        this.nameTeam = nameTeam;
        this.colorTeam = colorTeam;
        this.scoreTeam = scoreTeam;
    }
}

