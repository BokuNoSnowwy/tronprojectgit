﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameplayManagerTron : MonoBehaviour
{
    
    public static GameplayManagerTron Instance;
    
    public List<PlayerStatsTron> playerListController;
    public List<PlayerStatsTron> playerListKeyboard;
    public List<Teams> teamList = new List<Teams>();
    public List<Transform> respawnZoneList = new List<Transform>();
    
    public int scoreToWin;
    //Respawn Player 
    public float respawnTimePlayer;
    
    //UI Score
    [Header("UI Score In Game")]
    [SerializeField]private Transform whitePanelParent;
    [SerializeField]private GameObject scoreTextPrefab;
    [SerializeField]private GameObject textMeshProPrefab;
    
    //UI Panel
    [Header("UI Pause")]
    [SerializeField] private bool onPause;
    [SerializeField] private GameObject pausePanelUI;
    
    [Header("UI Win")]
    [SerializeField] private GameObject victoryPanelUI;
    [SerializeField] private GameObject verticalLayoutWin;
    [SerializeField] private GameObject teamScorePrefab;
    [SerializeField] private TextMeshProUGUI winLabelText;
    
    [SerializeField] public List<GameObject> meshBikeList = new List<GameObject>();
    
    //CenterMap
    public Transform centerMap;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        if (PlayerTeamManager.Instance)
        {
            //PopulateLists();
            List<Transform> listRespawn = new List<Transform>();
            listRespawn = new List<Transform>(respawnZoneList);

                       
            foreach (var integer in PlayerTeamManager.Instance.intListController)
            {
                Transform respawn = listRespawn[Random.Range(0, listRespawn.Count)];
                Instantiate(playerListController[integer], respawn.position, respawn.rotation);
                listRespawn.Remove(respawn);
            }
            
            foreach (var integer in PlayerTeamManager.Instance.intListKeyboard)
            {
                Transform respawn = listRespawn[Random.Range(0, listRespawn.Count)];
                Instantiate(playerListKeyboard[integer], respawn.position, respawn.rotation);
                listRespawn.Remove(respawn);
            }
        }
        
    }


    public void UpdateScore()
    {
        //TODO Update le score de toutes les équipes a chaque point marqué
        
        //Check if a team has the required nb of point in order to win 
        foreach (var team in teamList)
        {
            if (team.scoreTeam >= scoreToWin)
            {
                Win(team);
            }
        }
    }

    private void PopulateLists()
    {
        playerListController.Clear();
        teamList.Clear();
        
        if (PlayerTeamManager.Instance)
        {
            PlayerTeamManager playerTeamManager = PlayerTeamManager.Instance;
            foreach (var player in playerTeamManager.playerList)
            {
                playerListController.Add(player.GetComponent<PlayerStatsTron>());
            }

            foreach (var team in playerTeamManager.teamList)
            {
                teamList.Add(team);
            }
     
        }
    }
    
    //Restart de game
    public void RestartGame()
    {
        TimeManager.LocalTimeScale = 1;
        SceneManager.LoadScene("GameScene");
    }

    //Return to menu
    public void Menu()
    {
        TimeManager.LocalTimeScale = 1;
        SceneManager.LoadScene("MenuScene");
    }

    //Quit the game
    public void QuitGame()
    {
        Application.Quit();
    }
    

    //Stop the game
    public void StopGame()
    {
        TimeManager.LocalTimeScale = 0;
    }
    
    //Stop the game
    public void ResumeGame()
    {
        TimeManager.LocalTimeScale = 1;
    }

    //Display the win pannel of the winning team
    public void Win(Teams team)
    {
        //TODO gérer l'UI de victoire d'une équipe ici
        winLabelText.text = team.nameTeam + " has won !";
        Debug.Log(team.nameTeam + " has won !");
        StopGame();
        victoryPanelUI.SetActive(true);
        
        foreach (var teamGame in teamList)
        {
            GameObject prefabText = Instantiate(teamScorePrefab, verticalLayoutWin.transform);
            prefabText.GetComponent<TextMeshProUGUI>().text =
                teamGame.nameTeam + " : " + teamGame.scoreTeam + " Points";
        }
    }

    public void PauseGame()
    {
        if (!onPause)
        {
            pausePanelUI.SetActive(true);
            StopGame();
            onPause = true;
        }
        else
        {
            pausePanelUI.SetActive(false);
            ResumeGame();
            onPause = false;
        }
        
    }
    
    //When a player is killed, 
    public void DisablePlayer(PlayerStatsTron player)
    {
        player.gameObject.SetActive(false);
        StartCoroutine(RespawnPlayer(player.gameObject, respawnTimePlayer));
        //StartCoroutine(DisplayText("You are dead, respawn in 5 seconds", player.GetComponent<PlayerStatsTron>().team, 5f));
    }

    public void GivePoint(int point, Teams team)
    {
        team.scoreTeam += point;
        UpdateScore();
    }
    

    //Respawn to his base after a few seconds.
    IEnumerator RespawnPlayer(GameObject player,float respawnTime)
    {
        PlayerStatsTron playerStatsTron = player.GetComponent<PlayerStatsTron>();

        yield return new WaitForSeconds(respawnTime);
        
        Shuffle(respawnZoneList);
        foreach (var respawnZone in respawnZoneList)
        {
            LayerMask layer = LayerMask.GetMask("Trail");
            RaycastHit hit;
            if (!Physics.SphereCast(respawnZone.position, 20f, Vector3.zero, out hit,50f,layer))
            {
                Debug.Log(respawnZone.name);

                Debug.Log("No Trail Nearby");
                player.transform.position = respawnZone.transform.position;
                player.transform.rotation = respawnZone.transform.rotation;
                break;
            }
        }
        
        playerStatsTron.Respawn();
        player.SetActive(true);
    }
    
    //Function that will shuffle a list
    public static void Shuffle<T>(IList<T> ts) {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i) {
            var r = Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    /*
    //Display to the team the text in parameters for a defined number of seconds.
    public IEnumerator DisplayText(string text, Teams team, float displayTime)
    {
        GameObject info = Instantiate(textMeshProPrefab, team.teamInfoZone);
        info.GetComponent<TextMeshProUGUI>().text = text;
        yield return new WaitForSeconds(displayTime);
        Destroy(info);
    }
    */
}

