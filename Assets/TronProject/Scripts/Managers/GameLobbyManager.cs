﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameLobbyManager : MonoBehaviour
{
    public static GameLobbyManager Instance;
    
    [Header("PlayerSelection")]
    [SerializeField] private GameObject controllerWarningText;
    [SerializeField] private bool fullControllersLobby;

    
    [Header("TeamFormation")]
    [SerializeField] public int nbOfPlayer;
    [SerializeField] private GameObject panelNbPlayer;
    [SerializeField] private GameObject panelPlayerTeamSelection;

    
    [Header("TeamFormation Grid Layout")]
    [SerializeField] private GameObject gridLayoutGroup;
    [SerializeField] private List<GameObject> playerListPrefabs;
    
    [Header("Prefab")]
    [SerializeField] private GameObject playerPanelPrefabKeyboard;
    [SerializeField] private List<GameObject> playerPanelPrefabGamepad = new List<GameObject>();

    
    [Header("PlayerList")]
    [SerializeField] private List<GameObject> playerPrefabController = new List<GameObject>();
    [SerializeField] private List<GameObject> playerPrefabKeyboard = new List<GameObject>();
    [SerializeField] public List<GameObject> playerPrefabToLaunch = new List<GameObject>();
    [SerializeField] public List<Teams> teamListToLaunch = new List<Teams>();

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GameNumberOfPlayer(int playerNumber)
    {
        EnterTeamFormation(playerNumber,true);
        
        
        int connectedPlayer = Gamepad.all.Count +1;
        if (connectedPlayer >= playerNumber)
        {
            if (connectedPlayer -1 == playerNumber)
            {
                EnterTeamFormation(connectedPlayer -1 ,true);
            }
            else if (connectedPlayer - 2 == playerNumber)
            {
                EnterTeamFormation(connectedPlayer -2 ,false);
            }
            else
            {
                EnterTeamFormation(connectedPlayer,false);
            }

        }
        else if (connectedPlayer < playerNumber)
        {
            controllerWarningText.SetActive(true);
            StartCoroutine(CutAfterSeconds(5f, controllerWarningText));
        }
        
    }


    public IEnumerator CutAfterSeconds(float timer, GameObject gameObject)
    {
        yield return new WaitForSeconds(timer);
        gameObject.SetActive(false);
    }

    public void EnterTeamFormation(int nbOfPlayers, bool fullControllers)
    {
        Debug.Log(nbOfPlayers);
        panelNbPlayer.SetActive(false);
        panelPlayerTeamSelection.SetActive(true);
        fullControllersLobby = fullControllers;
        
        if (fullControllers)
        {
            for (int i = 0; i < nbOfPlayers; i++)
            {
                GameObject prefabPanelPlayerController = Instantiate(playerPanelPrefabGamepad[i], gridLayoutGroup.transform);
                prefabPanelPlayerController.GetComponent<PlayerPrefabUIManager>().textPlayer.text = "Controller : " + (i+1);
                playerListPrefabs.Add(prefabPanelPlayerController);
            }
        }
        else
        {
            GameObject prefabPanelPlayerKeyboard = Instantiate(playerPanelPrefabKeyboard, gridLayoutGroup.transform);
            prefabPanelPlayerKeyboard.GetComponent<PlayerPrefabUIManager>().textPlayer.text = "Keyboard : 1";
            playerListPrefabs.Add(prefabPanelPlayerKeyboard);
            for (int i = 0; i < nbOfPlayers - 1; i++)
            {
                GameObject prefabPanelPlayerGamepad = Instantiate(playerPanelPrefabGamepad[i], gridLayoutGroup.transform);
                prefabPanelPlayerGamepad.GetComponent<PlayerPrefabUIManager>().textPlayer.text = "Controller : " + (i+1);
                playerListPrefabs.Add(prefabPanelPlayerGamepad);
            }
        }
    }

    public void PopulatePlayersAndTeams()
    {
        int indexGamePad = 0;
        int indexKeyboard = 0;

        //Setup Players
        for (int i = 0; i < playerListPrefabs.Count; i++)
        {
            if (!playerListPrefabs[i].GetComponentInChildren<PlayerInput>() && !fullControllersLobby)
            {
                /*
                Debug.Log("Keyboard");
                PlayerPrefabUIManager playerPrefab =
                    playerListPrefabs[i].GetComponentInChildren<PlayerPrefabUIManager>();
                
                PlayerStatsTron playerStatsKeyboard = playerPrefabKeyboard[indexKeyboard].GetComponent<PlayerStatsTron>();
                
                TeamScriptable teamScriptable = playerPrefab.teamList[playerPrefab.teamIndex];
                BikeScriptable bikeScriptable = playerPrefab.bikeList[playerPrefab.bikeIndex];
            
                Teams teamPlayer = new Teams(playerPrefab.teamText.text,teamScriptable.color,0);
                //playerStatsKeyboard.team = teamPlayer;
                playerStatsKeyboard.bike = bikeScriptable;
                Debug.Log(bikeScriptable.name);
                playerStatsKeyboard.idBike = playerPrefab.bikeIndex;
                */
                
                //PlayerTeamManager.Instance.PopulatePlayerList(playerStatsKeyboard);
                PlayerTeamManager.Instance.intListKeyboard.Add(indexGamePad);
                indexKeyboard += 1;
            }
            else
            {
                /*
                PlayerPrefabUIManager playerPrefab =
                    playerListPrefabs[i].GetComponentInChildren<PlayerPrefabUIManager>();

                PlayerStatsTron playerStats = playerPrefabController[indexGamePad].GetComponent<PlayerStatsTron>();
                
                TeamScriptable teamScriptable = playerPrefab.teamList[playerPrefab.teamIndex];

                BikeScriptable bikeScriptable = playerPrefab.bikeList[playerPrefab.bikeIndex];

                Teams teamPlayer = new Teams(playerPrefab.teamText.text,teamScriptable.color,0);
                //playerStats.team = teamPlayer;
                playerStats.bike = bikeScriptable;
                playerStats.idBike = playerPrefab.bikeIndex;
                
                //playerStats.bike = bikeScriptable;
                */
                
                PlayerTeamManager.Instance.intListController.Add(indexGamePad);
                indexGamePad += 1;
            }
            
           
        }
        //Setup Teams : 
        SceneManager.LoadScene(1);
    }
    
}
