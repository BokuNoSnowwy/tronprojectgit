﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class UIButtonManagerMenu : MonoBehaviour
{

    [SerializeField] private GameObject controlsPanel;
    [SerializeField] private GameObject playPanel; 


    //Start the game
    public void Play()
    {
        if (!playPanel.activeSelf)
        {
            playPanel.SetActive(true);
        }
        else
        {
            playPanel.SetActive(false);
        }
    }

    //Show the control panel
    public void ShowControls()
    {
        if (!controlsPanel.activeSelf)
        {
            controlsPanel.SetActive(true);
        }
        else
        {
            controlsPanel.SetActive(false);
        }
    }

    //Quit the game
    public void Quit()
    {
        Application.Quit();
    }
    //Load the scene assigned
    public void LoadScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    private void Start()
    {
    }
}
