﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TimeManager
{
   public static float LocalTimeScale = 1f;
   public static float deltaTime => Time.deltaTime * LocalTimeScale;

   public static bool IsPaused()
   {
      return LocalTimeScale == 0f;
   }

   public static float TimeScale()
   {
      return Time.deltaTime * LocalTimeScale;
   }

   public static void PauseGame(bool value)
   {
      if (value)
      {
         LocalTimeScale = 0;
      }
      else
      {
         LocalTimeScale = 1;
      }
   }
}
