﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;


public class PlayerPrefabUIManager : MonoBehaviour
{
    public TextMeshProUGUI textPlayer;

    [Header("Team Management")]
    public TextMeshProUGUI teamText;
    public Image teamTextBackground;
    public int teamIndex;
    public List<TeamScriptable> teamList = new List<TeamScriptable>();
    
    [Header("Bike Management")]
    public int bikeIndex;
    public Image bikeImage;
    public TextMeshProUGUI bikeName;
    public List<BikeScriptable> bikeList = new List<BikeScriptable>();

    [Header("Input Management")] 
    public int indexSelection;
    public List<GameObject> outlineList = new List<GameObject>();
    public float horizontal;
    public float vertical;
    
    
    public void OnHorizontalMove(InputAction.CallbackContext ctx)
    {
        horizontal = ctx.ReadValue<float>();
        if (ctx.performed)
        {
            switch (horizontal)
            {
                case -1f :
                    switch (indexSelection)
                    {
                        case 0 :
                            NextTeam(-1);
                            break;
                        case 1 :
                            NextBike(-1);
                            break;
                    }
                    break;
            
                case 1f :
                    switch (indexSelection)
                    {
                        case 0 :
                            NextTeam(1);
                            break;
                        case 1 :
                            NextBike(-1);
                            break;
                    }
                    break;
            }
        }
    }

    public void OnVerticalMove(InputAction.CallbackContext ctx)
    {
        vertical = ctx.ReadValue<float>();
        if (ctx.performed)
        {
            switch (vertical)
            {
                case -1f :
                    Debug.Log("-1");
                    ChangeIndexSelection(1);
                    break;
            
                case 1f :
                    Debug.Log("+1");
                    ChangeIndexSelection(0);
                    break;
            }
        }
    }

    public void ChangeIndexSelection(int value)
    {
        foreach (var outline in outlineList)
        {
            outline.SetActive(false);
        }
        outlineList[value].SetActive(true);
        indexSelection = value;
    }


    // Start is called before the first frame update
    void Start()
    {
        RefreshTeam();
        RefreshBike();
        ChangeIndexSelection(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextTeam(int value)
    {
        if (value > 0)
        {
            if (teamIndex == 3)
            {
                teamIndex = 0;
            }
            else
            {
                teamIndex += 1;
            }
        }
        else
        {
            if (teamIndex == 0)
            {
                teamIndex = 3;
            }
            else
            {
                teamIndex -= 1;
            }
        }
        
        RefreshTeam();
    }
    
    public void NextBike(int value)
    {
        if (value > 0)
        {
            if (bikeIndex == 2)
            {
                bikeIndex = 0;
            }
            else
            {
                bikeIndex += 1;
            }
        }
        else
        {
            if (bikeIndex == 0)
            {
                bikeIndex = 2;
            }
            else
            {
                bikeIndex -= 1;
            }
        }
        RefreshBike();
    }

    public void RefreshTeam()
    {
        TeamScriptable team = teamList[teamIndex];
        teamText.text = "Team : " + team.idTeam;

        //Color Team
        var color = team.color;
        color.a = 1;
        teamTextBackground.color = color;
    }
    
    public void RefreshBike()
    {
        BikeScriptable bike = bikeList[bikeIndex];
        if (bike.bikeImage)
        {
            bikeImage.sprite = bike.bikeImage;  
        }
        bikeName.text = bike.name.ToString();
    }
}
