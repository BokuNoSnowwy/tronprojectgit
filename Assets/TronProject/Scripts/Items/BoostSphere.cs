﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostSphere : MonoBehaviour
{
    public float scaleMultiplier;
    public float turboGiven;

    private Vector3 basePosition;
    
    //Respawn
    public float timeRespawn;
    private SphereCollider _sphereCollider;
    private MeshRenderer _meshRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
        _sphereCollider = GetComponent<SphereCollider>();
        _meshRenderer = GetComponent<MeshRenderer>();
        transform.localScale = transform.localScale * scaleMultiplier;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerStatsTron>())
        {
            other.GetComponent<PlayerStatsTron>().AddTurbo(turboGiven);
            StartCoroutine(RespawnSphere(timeRespawn));
        }
    }

    public IEnumerator RespawnSphere(float timeRespawn)
    {
        Debug.Log("False");
        _sphereCollider.enabled = false;
        _meshRenderer.enabled = false;
        yield return new WaitForSeconds(timeRespawn);
        Debug.Log("Respawn");
        _sphereCollider.enabled = true;
        _meshRenderer.enabled = true;
    }
}
