﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ttest : MonoBehaviour
{
    public ParticleSystem particleSystem;

    [Range(0, 50)] public float rateOverTime;
    [Range(0, 20)] public float angle;

    void Start()
    {
       
    }
    
    void Update()
    {
        ParticleSystem.EmissionModule emissionModule =  particleSystem.emission;
        emissionModule.rateOverTime = rateOverTime;

        ParticleSystem.ShapeModule shapeModule = particleSystem.shape;
        shapeModule.angle = angle;
    }
}
