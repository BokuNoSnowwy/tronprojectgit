﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeDestruction : MonoBehaviour
{
    public List<GameObject> gameObjects;

    public float force;

    void Start()
    {
        Destruct();
    }
    
    void Update()
    {
        
    }

    public void Destruct()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            gameObjects.Add(transform.GetChild(i).gameObject);
        }
        
        foreach (var go in gameObjects)
        {
            Rigidbody rigi = go.AddComponent<Rigidbody>();
            
            rigi.AddForce(Vector3.up * force);
            
            Destroy(go, 3f);
        }
    }
}
